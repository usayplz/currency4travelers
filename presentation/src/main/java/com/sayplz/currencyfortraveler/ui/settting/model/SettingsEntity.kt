package com.sayplz.currencyfortraveler.ui.settting.model

import android.support.annotation.DrawableRes
import java.math.BigDecimal

data class SettingsEntity(
        val entityId: Long,
        val code: String,
        val name: String,
        @DrawableRes val flag: Int,
        val price: BigDecimal,
        private val isEntityFavorite: Boolean,
        val isSelected: Boolean,
        val favoritePosition: Long
) : BaseSettingsEntity(entityId, isEntityFavorite)