package com.sayplz.currencyfortraveler.ui.currency

import android.graphics.Canvas
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View

/**
 * Created by Sergei Kurikalov
 * on 22.05.18.
 */
class ItemTouchHelperCallback(private val adapter: ItemTouchHelperAdapter,
                              private val isLongPressDragEnabled: Boolean = false,
                              private val isItemViewSwipeEnabled: Boolean = true) : ItemTouchHelper.Callback() {

    override fun isLongPressDragEnabled(): Boolean = isLongPressDragEnabled

    override fun isItemViewSwipeEnabled(): Boolean = isItemViewSwipeEnabled

    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
        val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN
        val swipeFlags = ItemTouchHelper.END
        return ItemTouchHelper.Callback.makeMovementFlags(dragFlags, swipeFlags)
    }

    override fun onMove(recyclerView: RecyclerView, source: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        adapter.onItemMove(source.adapterPosition, target.adapterPosition)
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, i: Int) {
        adapter.onItemDismiss(viewHolder.adapterPosition)
    }

    override fun onChildDrawOver(canvas: Canvas, recyclerView: RecyclerView,
                                 viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float,
                                 actionState: Int, isCurrentlyActive: Boolean) {
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            ItemTouchHelper.Callback.getDefaultUIUtil().onDrawOver(canvas, recyclerView,
                    getForegroundView(viewHolder), dX, dY, actionState, isCurrentlyActive)
        } else {
            super.onChildDrawOver(canvas, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        }
    }

    override fun onChildDraw(canvas: Canvas, recyclerView: RecyclerView,
                             viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float,
                             actionState: Int, isCurrentlyActive: Boolean) {
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            ItemTouchHelper.Callback.getDefaultUIUtil().onDraw(canvas, recyclerView,
                    getForegroundView(viewHolder), dX, dY, actionState, isCurrentlyActive)
        } else {
            super.onChildDraw(canvas, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        }
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            getDefaultUIUtil().onSelected(getForegroundView(viewHolder))
        } else {
            super.onSelectedChanged(viewHolder, actionState)
        }
    }

    override fun clearView(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder) {
        getDefaultUIUtil().clearView(getForegroundView(viewHolder))
        super.clearView(recyclerView, viewHolder)
    }

    private fun getForegroundView(viewHolder: RecyclerView.ViewHolder?): View {
        return (viewHolder as CurrencyAdapter.ViewHolder).binding.viewForeground
    }

    interface ItemTouchHelperAdapter {
        fun onItemMove(fromPosition: Int, toPosition: Int)
        fun onItemDismiss(position: Int)
    }

    interface ItemTouchHelperViewHolder {
        fun onItemSelected()
        fun onItemClear()
    }
}
