package com.sayplz.currencyfortraveler.ui.content

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.sayplz.currencyfortraveler.R
import com.sayplz.currencyfortraveler.ui.currency.CurrencyViewModel
import com.sayplz.currencyfortraveler.ui.settting.SettingsViewModel
import com.sayplz.currencyfortraveler.util.hideKeyboard
import com.sayplz.currencyfortraveler.util.onPageChangeListener
import com.sayplz.data.common.doNothing
import kotlinx.android.synthetic.main.activity_content.*
import org.koin.android.architecture.ext.viewModel


class ContentActivity : AppCompatActivity() {
    private val currencyViewModel: CurrencyViewModel by viewModel()
    private val settingsViewModel: SettingsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)
        setupViewPager()
    }


    override fun onBackPressed() {
        if (viewPager.currentItem == ContentPagerAdapter.SETTINGS_POSITION) {
            selectCurrencyFragment()
        } else {
            super.onBackPressed()
        }
    }

    private fun setupViewPager() {
        val adapter = ContentPagerAdapter(supportFragmentManager)
        viewPager.adapter = adapter

        viewPager.onPageChangeListener(
                onPageScrollStateChanged = { hideKeyboard() },
                onPageSelected = { refreshCurrencies(it) })
    }

    private fun refreshCurrencies(position: Int) = when (position) {
        ContentPagerAdapter.CURRENCY_POSITION -> currencyViewModel.favorites.refresh()
        ContentPagerAdapter.SETTINGS_POSITION -> settingsViewModel.currencies.refresh()
        else -> doNothing()
    }

    fun selectCurrencyFragment() {
        viewPager.setCurrentItem(ContentPagerAdapter.CURRENCY_POSITION, true)
    }

    fun selectSettingsFragment() {
        viewPager.setCurrentItem(ContentPagerAdapter.SETTINGS_POSITION, true)
    }
}
