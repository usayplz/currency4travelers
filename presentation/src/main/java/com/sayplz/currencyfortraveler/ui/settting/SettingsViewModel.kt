package com.sayplz.currencyfortraveler.ui.settting

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableBoolean
import android.widget.EditText
import com.sayplz.currencyfortraveler.common.UiMapper
import com.sayplz.currencyfortraveler.common.UiResultLiveDataWithCoroutine
import com.sayplz.currencyfortraveler.ui.settting.model.BaseSettingsEntity
import com.sayplz.domain.entity.CurrencyEntity
import com.sayplz.domain.usecase.AddFavoriteCurrencyUseCase
import com.sayplz.domain.usecase.RemoveFavoriteCurrencyUseCase
import com.sayplz.domain.usecase.SearchCurrenciesUseCase
import kotlinx.coroutines.experimental.launch

class SettingsViewModel(private val mapper: UiMapper,
                        private val searchCurrencies: SearchCurrenciesUseCase,
                        private val addFavorite: AddFavoriteCurrencyUseCase,
                        private val removeFavorite: RemoveFavoriteCurrencyUseCase) : ViewModel() {

    val currencies = getFilteredCurrencies()

    var isClearVisible = ObservableBoolean(false)

    var query = ""
        set(value) {
            isClearVisible.set(value.isNotEmpty())
            field = value
            currencies.refresh()
        }

    fun clearSearchText(search: EditText) {
        search.setText("")
        currencies.refresh()
    }

    private fun getFilteredCurrencies() = UiResultLiveDataWithCoroutine<List<CurrencyEntity>, List<BaseSettingsEntity>>(
            transform = {
                if (query.isEmpty()) {
                    mapper.toSettingsEntityWithFavorites(it)
                } else {
                    mapper.toSettingsEntity(it)
                }
            },
            exec = { searchCurrencies.execute(query) })

    fun addFavorites(id: Long) = launch {
        addFavorite.execute(id)
        currencies.refresh()
    }

    fun removeFavorites(id: Long) = launch {
        removeFavorite.execute(id)
        currencies.refresh()
    }
}
