package com.sayplz.currencyfortraveler.ui.currency

import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.MotionEvent
import android.view.View
import com.android.databinding.library.baseAdapters.BR
import com.sayplz.currencyfortraveler.ui.currency.model.CurrencyUi

/**
 * Created by Sergei Kurikalov
 * on 22.05.18.
 */
abstract class BaseCurrencyViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root),
        ItemTouchHelperCallback.ItemTouchHelperViewHolder {

    override fun onItemSelected() {
        binding.root.alpha = 0.5f
    }

    override fun onItemClear() {
        binding.root.alpha = 1f
    }

    abstract fun getHandler(): View

    fun bind(item: CurrencyUi, listener: CurrencyAdapter.CurrencyAdapterListener) {
        binding.setVariable(BR.item, item)
        binding.root.setOnClickListener { listener.onSelect(item) }
        binding.executePendingBindings()
        setOnHandlerTouch(listener)
    }

    private fun setOnHandlerTouch(listener: CurrencyAdapter.CurrencyAdapterListener) {
        getHandler().setOnTouchListener { _, event ->
            if (event.actionMasked == MotionEvent.ACTION_DOWN) {
                listener.onStartDrag(this@BaseCurrencyViewHolder)
            }
            return@setOnTouchListener false
        }
    }
}