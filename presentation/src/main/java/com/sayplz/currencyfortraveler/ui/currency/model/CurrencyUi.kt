package com.sayplz.currencyfortraveler.ui.currency.model

import android.support.annotation.DrawableRes
import com.sayplz.domain.entity.CalculatedDataEntity
import java.math.BigDecimal

data class CurrencyUi(
        val id: Long,
        val code: String,
        val name: String,
        @DrawableRes val flag: Int,
        val price: BigDecimal,
        val isFavorite: Boolean,
        val isSelected: Boolean,
        val favoritePosition: Long,
        val calculatedData: CalculatedDataEntity
)
