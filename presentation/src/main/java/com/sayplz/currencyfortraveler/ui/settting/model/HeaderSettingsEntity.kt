package com.sayplz.currencyfortraveler.ui.settting.model

import com.brandongogetap.stickyheaders.exposed.StickyHeader

/**
 * Created by Sergei Kurikalov
 * on 22.05.18.
 */
data class HeaderSettingsEntity(val headerId: Long, val name: String)
    : BaseSettingsEntity(headerId, false), StickyHeader