package com.sayplz.currencyfortraveler.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.LinearLayout
import com.sayplz.currencyfortraveler.R

class CursorWidget @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : LinearLayout(context, attrs, defStyleAttr) {

    companion object {
        private const val DURATION = 550L
        private const val FROM_ALPHA = 0.1f
        private const val TO_ALPHA = 0.7f
    }

    init {
        LayoutInflater.from(context).inflate(R.layout.widget_cursor, this, true)
        this.startAnimation(alphaAnimation())
    }

    private fun alphaAnimation() = AlphaAnimation(FROM_ALPHA, TO_ALPHA).apply {
        duration = DURATION
        interpolator = AccelerateDecelerateInterpolator()
        repeatCount = Animation.INFINITE
        repeatMode = Animation.REVERSE
    }

    override fun setVisibility(visibility: Int) {
        if (visibility == View.GONE || visibility == View.INVISIBLE) {
            this.clearAnimation()
        } else {
            this.startAnimation(alphaAnimation())
        }

        super.setVisibility(visibility)
    }
}
