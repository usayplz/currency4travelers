package com.sayplz.currencyfortraveler.ui.settting

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sayplz.currencyfortraveler.R
import com.sayplz.currencyfortraveler.common.UiResult
import com.sayplz.currencyfortraveler.databinding.FragmentSettingsBinding
import com.sayplz.currencyfortraveler.ui.content.ContentActivity
import com.sayplz.currencyfortraveler.ui.settting.model.BaseSettingsEntity
import com.sayplz.currencyfortraveler.util.init
import com.sayplz.currencyfortraveler.util.observe
import com.sayplz.data.common.doNothing
import org.koin.android.architecture.ext.sharedViewModel


class SettingsFragment : Fragment() {
    private lateinit var binding: FragmentSettingsBinding
    private val viewModel: SettingsViewModel by sharedViewModel()
    private val settingAdapter = SettingAdapter { setFavorite(it) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupBindings()
        setupViewModel()
    }

    private fun setupRecyclerView() {
        context?.let { context ->
            val layoutManager = TopSnappedStickyLayoutManager(context, settingAdapter)
            layoutManager.elevateHeaders(true)

            binding.currencyList.init(settingAdapter, layoutManager)
        }
    }

    private fun setupBindings() {
        binding.setLifecycleOwner(this)
        binding.viewModel = viewModel
        binding.executePendingBindings()

        binding.back.setOnClickListener { (activity as ContentActivity).selectCurrencyFragment() }
    }

    private fun setupViewModel() {
        viewModel.currencies.observe(this) { showCurrencies(it) }
    }

    private fun showCurrencies(uiResult: UiResult<List<BaseSettingsEntity>>?) = when (uiResult) {
        is UiResult.OnSuccess -> settingAdapter.submitList(uiResult.data?.toMutableList())
        else -> doNothing()
    }

    private fun setFavorite(currencyEntity: BaseSettingsEntity) {
        if (currencyEntity.isFavorite) {
            viewModel.removeFavorites(currencyEntity.id)
        } else {
            viewModel.addFavorites(currencyEntity.id)
        }
    }
}
