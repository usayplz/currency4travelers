package com.sayplz.currencyfortraveler.ui.about

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sayplz.currencyfortraveler.R
import com.sayplz.currencyfortraveler.util.string
import com.sayplz.currencyfortraveler.util.toastInfo
import kotlinx.android.synthetic.main.fragment_about_dialog.view.*


class AboutDialogFragment : DialogFragment() {
    companion object {
        fun newInstance(): AboutDialogFragment {
            return AboutDialogFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_about_dialog, container, false)
        view.close.setOnClickListener { dismiss() }
        view.pal_link.setOnClickListener { openPaypalLinkInWeb() }
        view.btc_link.setOnClickListener { copyToClipboard(R.string.about_btc) }
        view.eth_link.setOnClickListener { copyToClipboard(R.string.about_eth) }
        return view
    }

    private fun openPaypalLinkInWeb() {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(string(R.string.about_paypal)))
        startActivity(browserIntent)
    }

    private fun copyToClipboard(@StringRes link: Int) {
        context?.let { context ->
            val clipData = ClipData.newPlainText(string(R.string.app_name), string(link))
            (context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager).primaryClip = clipData
            context.toastInfo(R.string.copied)
        }
    }
}
