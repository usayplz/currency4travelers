package com.sayplz.currencyfortraveler.ui.currency

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.sayplz.currencyfortraveler.R
import com.sayplz.currencyfortraveler.databinding.CurrencyItemBinding
import com.sayplz.currencyfortraveler.ui.currency.model.CurrencyUi
import com.sayplz.currencyfortraveler.util.databind
import java.util.*

class CurrencyAdapter(private val listener: CurrencyAdapterListener)
    : ListAdapter<CurrencyUi, RecyclerView.ViewHolder>(diffCallback),
        ItemTouchHelperCallback.ItemTouchHelperAdapter {

    private var items: MutableList<CurrencyUi>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(parent.databind(viewType))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(getItem(position), listener)
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.currency_item
    }

    override fun getItemId(position: Int): Long {
        return super.getItem(position).id
    }

    override fun submitList(items: MutableList<CurrencyUi>?) {
        this.items = items
        super.submitList(this.items)
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int) {
        Collections.swap(items, fromPosition, toPosition)
        notifyItemMoved(fromPosition, toPosition)
        listener.onSwap(getItem(fromPosition), getItem(toPosition))
    }

    override fun onItemDismiss(position: Int) {
        listener.onRemove(getItem(position))
        items?.removeAt(position)
        notifyItemRemoved(position)
    }

    class ViewHolder(val binding: CurrencyItemBinding) : BaseCurrencyViewHolder(binding) {
        override fun getHandler(): View = binding.handler
    }

    interface CurrencyAdapterListener {
        fun onSelect(currency: CurrencyUi)
        fun onStartDrag(holder: RecyclerView.ViewHolder)
        fun onSwap(from: CurrencyUi, to: CurrencyUi)
        fun onRemove(currency: CurrencyUi)
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<CurrencyUi>() {
            override fun areItemsTheSame(old: CurrencyUi, new: CurrencyUi) = old.id == new.id
            override fun areContentsTheSame(old: CurrencyUi, new: CurrencyUi) = old == new
        }
    }
}
