package com.sayplz.currencyfortraveler.ui.currency

import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.sayplz.currencyfortraveler.R
import com.sayplz.currencyfortraveler.ui.currency.model.Buttons
import com.sayplz.currencyfortraveler.util.dimen
import com.sayplz.currencyfortraveler.util.inflate


class ButtonsAdapter(private val onButtonClick: (Buttons) -> Unit) : BaseAdapter() {
    override fun getCount(): Int = Buttons.values().size - 1

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getItem(position: Int): Buttons = Buttons.values()[position]

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return convertView ?: createButton(getItem(position), parent)
    }

    private fun createButton(button: Buttons, viewGroup: ViewGroup?): View {
        return when (button) {
            Buttons.BACKSPACE -> drawImageButton(viewGroup, R.drawable.ic_backspace)
            else -> drawTextButton(viewGroup, button)
        }
    }

    private fun drawImageButton(viewGroup: ViewGroup?, imageRes: Int) =
            (viewGroup.inflate(R.layout.button_image_item) as ImageButton).apply {
                setImageResource(imageRes)
                layoutParams = LinearLayout.LayoutParams(GridView.AUTO_FIT, dimen(R.dimen.grid_button_height))
                setOnClickListener { onButtonClick(Buttons.BACKSPACE) }
                setOnLongClickListener {
                    onButtonClick(Buttons.LONG_BACKSPACE)
                    return@setOnLongClickListener true
                }
            }

    private fun drawTextButton(viewGroup: ViewGroup?, button: Buttons) =
            (viewGroup.inflate(R.layout.button_text_item) as Button).apply {
                text = button.symbol
                layoutParams = LinearLayout.LayoutParams(GridView.AUTO_FIT, dimen(R.dimen.grid_button_height))
                setOnClickListener { onButtonClick(button) }
            }
}
