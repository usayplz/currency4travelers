package com.sayplz.currencyfortraveler.ui.currency

import android.arch.lifecycle.ViewModel
import com.sayplz.currencyfortraveler.common.UiMapper
import com.sayplz.currencyfortraveler.common.UiResultLiveDataWithCoroutine
import com.sayplz.currencyfortraveler.ui.currency.model.Buttons
import com.sayplz.currencyfortraveler.ui.currency.model.CurrencyUi
import com.sayplz.domain.entity.CurrencyEntity
import com.sayplz.domain.usecase.*
import kotlinx.coroutines.experimental.launch

class CurrencyViewModel(private val mapper: UiMapper,
                        private val getFavoriteCurrencies: GetFavoriteCurrenciesUseCase,
                        private val addButton: AddButtonUseCase,
                        private val getInitialCalculatedData: GetInitialCalculatedDataUseCase,
                        private val selectCurrency: SelectCurrencyUseCase,
                        private val swapFavoriteCurrencyPositions: SwapFavoriteCurrencyPositionsUseCase,
                        private val removeFavoriteCurrency: RemoveFavoriteCurrencyUseCase,
                        private val updateRatio: UpdateRatioUseCase) : ViewModel() {

    private var calculatedData = getInitialCalculatedData.execute()

    val favorites = getFavoritesUiResult()
    val updateRatios = updateRatios()

    fun onButtonClick(button: Buttons) = launch {
        calculatedData = addButton.execute(mapper.toEntity(button))
        favorites.refresh()
    }

    fun onSelect(currencyUi: CurrencyUi) = launch {
        selectCurrency.execute(currencyUi.id)
        calculatedData = getInitialCalculatedData.execute()
        favorites.refresh()
    }

    private fun getFavoritesUiResult() = UiResultLiveDataWithCoroutine<List<CurrencyEntity>, List<CurrencyUi>>(
            transform = { mapper.toUi(it, calculatedData) },
            exec = { getFavoriteCurrencies.execute() })

    private fun updateRatios() = UiResultLiveDataWithCoroutine<Boolean, Boolean>(
            isStartOnObserve = true,
            isStartOnActive = false,
            transform = { return@UiResultLiveDataWithCoroutine it },
            exec = { updateRatio.execute().also { favorites.refresh() } })

    fun swap(itemFrom: CurrencyUi, itemTo: CurrencyUi) = launch {
        swapFavoriteCurrencyPositions.execute(itemFrom.id to itemTo.id)
    }

    fun remove(currency: CurrencyUi) = launch {
        removeFavoriteCurrency.execute(currency.id)
        favorites.refresh()
    }

    fun getDisplayValue(): String = when (calculatedData.initialValue) {
        "" -> calculatedData.displayValue
        else -> calculatedData.initialValue
    }
}
