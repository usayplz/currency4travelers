package com.sayplz.currencyfortraveler.ui.settting

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.android.databinding.library.baseAdapters.BR
import com.brandongogetap.stickyheaders.exposed.StickyHeaderHandler
import com.sayplz.currencyfortraveler.R
import com.sayplz.currencyfortraveler.databinding.HeaderSettingItemBinding
import com.sayplz.currencyfortraveler.databinding.SettingItemBinding
import com.sayplz.currencyfortraveler.ui.settting.model.BaseSettingsEntity
import com.sayplz.currencyfortraveler.ui.settting.model.HeaderSettingsEntity
import com.sayplz.currencyfortraveler.ui.settting.model.SettingsEntity
import com.sayplz.currencyfortraveler.util.databind

/**
 * Created by Sergei Kurikalov
 * on 17.05.18.
 */
class SettingAdapter(private val setFavorite: (BaseSettingsEntity) -> Unit)
    : ListAdapter<BaseSettingsEntity, RecyclerView.ViewHolder>(diffCallback), StickyHeaderHandler {

    private var items: MutableList<BaseSettingsEntity> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = when (viewType) {
        R.layout.header_setting_item -> HeaderViewHolder(parent.databind(viewType))
        R.layout.setting_item -> ViewHolder(parent.databind(viewType))
        else -> throw IllegalAccessError()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = when (getItem(position)) {
        is HeaderSettingsEntity -> (holder as HeaderViewHolder).bind(getItem(position))
        is SettingsEntity -> (holder as ViewHolder).bind(getItem(position), setFavorite)
        else -> throw IllegalAccessError()
    }

    override fun getItemViewType(position: Int): Int = when (getItem(position)) {
        is HeaderSettingsEntity -> R.layout.header_setting_item
        is SettingsEntity -> R.layout.setting_item
        else -> throw IllegalAccessError()
    }

    override fun submitList(items: MutableList<BaseSettingsEntity>?) {
        this.items = items ?: mutableListOf()
        super.submitList(this.items)
    }

    override fun getAdapterData(): MutableList<BaseSettingsEntity> {
        return items
    }

    class ViewHolder(private val binding: SettingItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: BaseSettingsEntity, setFavorite: (BaseSettingsEntity) -> Unit) {
            binding.setVariable(BR.item, item)
            binding.favorite.setOnClickListener { setFavorite(item) }
            binding.executePendingBindings()
        }
    }

    class HeaderViewHolder(private val binding: HeaderSettingItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: BaseSettingsEntity) {
            binding.setVariable(BR.item, item)
            binding.executePendingBindings()
        }
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<BaseSettingsEntity>() {
            override fun areItemsTheSame(old: BaseSettingsEntity, new: BaseSettingsEntity) = old.id == new.id
            override fun areContentsTheSame(old: BaseSettingsEntity, new: BaseSettingsEntity) = old == new
        }
    }
}
