package com.sayplz.currencyfortraveler.ui.currency

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import com.sayplz.currencyfortraveler.R
import com.sayplz.currencyfortraveler.common.UiResult
import com.sayplz.currencyfortraveler.databinding.FragmentCurrencyBinding
import com.sayplz.currencyfortraveler.ui.about.AboutDialogFragment
import com.sayplz.currencyfortraveler.ui.content.ContentActivity
import com.sayplz.currencyfortraveler.ui.currency.model.CurrencyUi
import com.sayplz.currencyfortraveler.util.*
import com.sayplz.data.common.doNothing
import com.sayplz.data.exception.FirstUpdateException
import org.koin.android.architecture.ext.sharedViewModel


class CurrencyFragment : Fragment() {
    private lateinit var binding: FragmentCurrencyBinding
    private lateinit var itemTouchHelper: ItemTouchHelper
    private val viewModel: CurrencyViewModel by sharedViewModel()
    private val currencyAdapter: CurrencyAdapter by lazy { setupCurrencyAdapter() }
    private var isUpdating: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_currency, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupItemTouchHelper()
        setupButtons()
        setupBindings()
        setupViewModel()
    }

    private fun setupCurrencyAdapter(): CurrencyAdapter {
        return CurrencyAdapter(setupCurrencyAdapterListener()).also { it.setHasStableIds(true) }
    }

    private fun setupCurrencyAdapterListener(): CurrencyAdapter.CurrencyAdapterListener {
        return object : CurrencyAdapter.CurrencyAdapterListener {
            override fun onSelect(currency: CurrencyUi) {
                viewModel.onSelect(currency)
            }

            override fun onStartDrag(holder: RecyclerView.ViewHolder) {
                itemTouchHelper.startDrag(holder)
            }

            override fun onSwap(from: CurrencyUi, to: CurrencyUi) {
                viewModel.swap(from, to)
            }

            override fun onRemove(currency: CurrencyUi) {
                viewModel.remove(currency)
            }
        }
    }

    private fun setupRecyclerView() {
        binding.currencyList.init(currencyAdapter)
    }

    private fun setupItemTouchHelper() {
        val itemTouchHelperCallback = ItemTouchHelperCallback(currencyAdapter)
        itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
        itemTouchHelper.attachToRecyclerView(binding.currencyList)
    }

    private fun setupButtons() {
        binding.buttons.adapter = ButtonsAdapter { viewModel.onButtonClick(it) }
    }

    private fun setupBindings() {
        binding.setLifecycleOwner(this)
        binding.viewModel = viewModel
        binding.executePendingBindings()

        binding.copy.setOnClickListener { copyValueToClipboard() }
        binding.help.setOnClickListener {
            AboutDialogFragment.newInstance().show(fragmentManager, AboutDialogFragment::class.java.name)
        }
        binding.update.setOnClickListener { updateRatios() }
        binding.settings.setOnClickListener { (activity as ContentActivity).selectSettingsFragment() }
    }

    private fun updateRatios() {
        viewModel.updateRatios.refresh()
        isUpdating = true
        binding.update.startAnimation(getRotateAnimation())
    }

    private fun getRotateAnimation(): RotateAnimation {
        return RotateAnimation(0f, 360f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f).apply {
            duration = 500
            repeatCount = Animation.INFINITE
        }
    }

    private fun copyValueToClipboard() {
        context?.let { context ->
            val clipData = ClipData.newPlainText(string(R.string.app_name), viewModel.getDisplayValue())
            (context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager).primaryClip = clipData
            context.toastInfo(R.string.copied)
        }
    }

    private fun setupViewModel() {
        viewModel.favorites.observe(this) { showFavorites(it) }
        viewModel.updateRatios.observe(this) { showUpdateResult(it) }
    }

    private fun showUpdateResult(uiResult: UiResult<Boolean>?) {
        when (uiResult) {
            is UiResult.OnSuccess -> onUpdatedSuccess(uiResult.data)
            is UiResult.OnFailure -> onUpdatedFailure(uiResult.throwable)
            is UiResult.OnFinish -> binding.update.clearAnimation().also { isUpdating = false }
            else -> doNothing()
        }
    }

    private fun onUpdatedSuccess(isFirstTimeUpdating: Boolean?) {
        if (isUpdating || isFirstTimeUpdating == true) {
            context?.toastSuccess(R.string.update_success)
        }
    }

    private fun onUpdatedFailure(throwable: Throwable?) {
        if (isUpdating || throwable is FirstUpdateException) {
            context?.toastError(R.string.update_failed)
        }
    }

    private fun showFavorites(uiResult: UiResult<List<CurrencyUi>>?) = when (uiResult) {
        is UiResult.OnSuccess -> currencyAdapter.submitList(uiResult.data?.toMutableList())
        else -> doNothing()
    }
}
