package com.sayplz.currencyfortraveler.ui.settting

import android.content.Context
import com.brandongogetap.stickyheaders.StickyLayoutManager
import com.brandongogetap.stickyheaders.exposed.StickyHeaderHandler

/**
 * Created by Sergei Kurikalov
 * on 22.05.18.
 */
class TopSnappedStickyLayoutManager(context: Context, headerHandler: StickyHeaderHandler) : StickyLayoutManager(context, headerHandler) {
    override fun scrollToPosition(position: Int) {
        super.scrollToPositionWithOffset(position, 0)
    }
}
