package com.sayplz.currencyfortraveler.ui.settting.model

abstract class BaseSettingsEntity(val id: Long, val isFavorite: Boolean)