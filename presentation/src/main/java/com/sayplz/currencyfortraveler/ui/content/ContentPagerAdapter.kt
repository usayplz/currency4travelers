package com.sayplz.currencyfortraveler.ui.content

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.sayplz.currencyfortraveler.ui.currency.CurrencyFragment
import com.sayplz.currencyfortraveler.ui.settting.SettingsFragment

class ContentPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
    companion object {
        const val CURRENCY_POSITION = 0
        const val SETTINGS_POSITION = 1
    }

    override fun getItem(position: Int): Fragment = when (position) {
        CURRENCY_POSITION -> CurrencyFragment()
        SETTINGS_POSITION -> SettingsFragment()
        else -> throw IndexOutOfBoundsException()
    }

    override fun getCount() = 2
}
