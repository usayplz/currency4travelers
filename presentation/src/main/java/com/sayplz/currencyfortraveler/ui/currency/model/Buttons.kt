package com.sayplz.currencyfortraveler.ui.currency.model

import java.text.DecimalFormatSymbols
import java.util.*

private val DECIMAL_SEPARATOR: String = DecimalFormatSymbols(Locale.getDefault()).decimalSeparator.toString()

enum class Buttons(val symbol: String) {
    BUTTON7("7"),
    BUTTON8("8"),
    BUTTON9("9"),
    DIVIDE("÷"),

    BUTTON4("4"),
    BUTTON5("5"),
    BUTTON6("6"),
    MULTIPLY("×"),

    BUTTON1("1"),
    BUTTON2("2"),
    BUTTON3("3"),
    MINUS("-"),

    COMMA(DECIMAL_SEPARATOR),
    BUTTON0("0"),
    BACKSPACE("\u232b"),
    PLUS("+"),
    LONG_BACKSPACE(""),
}
