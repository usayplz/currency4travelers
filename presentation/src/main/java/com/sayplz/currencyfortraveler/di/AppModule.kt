package com.sayplz.currencyfortraveler.di

import android.arch.persistence.room.Room
import android.content.Context
import com.sayplz.currencyfortraveler.common.UiMapper
import com.sayplz.currencyfortraveler.ui.currency.CurrencyViewModel
import com.sayplz.currencyfortraveler.ui.settting.SettingsViewModel
import com.sayplz.data.CurrencyRepositoryImpl
import com.sayplz.data.SessionRepositoryImpl
import com.sayplz.data.common.Constants
import com.sayplz.data.common.Executors
import com.sayplz.data.common.InitialDatabaseData
import com.sayplz.data.local.*
import com.sayplz.data.remote.FirebaseCurrencySource
import com.sayplz.domain.repository.CurrencyRepository
import com.sayplz.domain.repository.SessionRepository
import com.sayplz.domain.usecase.*
import org.koin.android.architecture.ext.viewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.applicationContext

val currencyModule = applicationContext {
    bean { Executors() }
    bean { createDiskDatabase(androidApplication()) }
    bean { createMemoryDataBase(androidApplication()) }
    bean { UiMapper(androidApplication()) }

    factory { DiskCurrencyLocalSource(get(), get()) }
    factory { MemoryCurrencyLocalSource(get(), get()) }
    factory { FirebaseCurrencySource() }
    factory { DiskSessionLocalSource(get(), get()) }

    bean { CurrencyRepositoryImpl(get(), get(), get(), get()) } bind CurrencyRepository::class
    bean { SessionRepositoryImpl(get()) } bind SessionRepository::class

    factory { RemoveFavoriteCurrencyUseCase(get()) }

    context(Contexts.FAVORITES) {
        factory { GetFavoriteCurrenciesUseCase(get()) }
        factory { UpdateRatioUseCase(get()) }
        factory { GetLastUpdateUseCase(get()) }
        factory { AddButtonUseCase(get()) }
        factory { SelectCurrencyUseCase(get()) }
        factory { GetInitialCalculatedDataUseCase(get()) }
        factory { SwapFavoriteCurrencyPositionsUseCase(get()) }

        viewModel { CurrencyViewModel(get(), get(), get(), get(), get(), get(), get(), get()) }
    }

    context(Contexts.SETTINGS) {
        factory { SearchCurrenciesUseCase(get()) }
        factory { AddFavoriteCurrencyUseCase(get()) }

        viewModel { SettingsViewModel(get(), get(), get(), get()) }
    }
}

fun createDiskDatabase(context: Context): DiskDatabase = Room
        .databaseBuilder(context, DiskDatabase::class.java, Constants.DATABASE_NAME)
        .addCallback(InitialDatabaseData().onCreateCallback())
        .fallbackToDestructiveMigration()
        .build()

fun createMemoryDataBase(context: Context): MemoryDatabase = Room
        .inMemoryDatabaseBuilder(context, MemoryDatabase::class.java)
        .build()

object Contexts {
    const val FAVORITES = "favorites"
    const val SETTINGS = "settings"
}
