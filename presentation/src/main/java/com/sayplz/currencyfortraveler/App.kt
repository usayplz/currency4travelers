package com.sayplz.currencyfortraveler

import android.app.Application
import android.support.v7.app.AppCompatDelegate
import org.koin.android.ext.android.startKoin
import com.sayplz.currencyfortraveler.di.currencyModule

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        setupVectorDrawable()
        setupKoin()
    }

    private fun setupKoin() {
        startKoin(this, listOf(currencyModule))
    }

    private fun setupVectorDrawable() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }
}
