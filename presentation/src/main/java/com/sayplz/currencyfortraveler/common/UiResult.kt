package com.sayplz.currencyfortraveler.common

sealed class UiResult<out T : Any> {
    class OnStart : UiResult<Nothing>()
    class OnSuccess<out T : Any>(val data: T?) : UiResult<T>()
    class OnFailure(val throwable: Throwable?) : UiResult<Nothing>()
    class OnFinish(val isCanceled: Boolean = false) : UiResult<Nothing>()
}
