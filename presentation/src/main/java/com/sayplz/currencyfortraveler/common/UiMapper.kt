package com.sayplz.currencyfortraveler.common

import android.content.Context
import com.sayplz.currencyfortraveler.ui.currency.model.Buttons
import com.sayplz.currencyfortraveler.ui.currency.model.CurrencyUi
import com.sayplz.currencyfortraveler.ui.settting.model.BaseSettingsEntity
import com.sayplz.currencyfortraveler.ui.settting.model.HeaderSettingsEntity
import com.sayplz.currencyfortraveler.ui.settting.model.SettingsEntity
import com.sayplz.domain.entity.ButtonsEntity
import com.sayplz.domain.entity.ButtonsTypeEntity.*
import com.sayplz.domain.entity.CalculatedDataEntity
import com.sayplz.domain.entity.CurrencyEntity
import java.math.BigDecimal

class UiMapper(private val context: Context) {
    companion object {
        private const val FAVORITE_SYMBOL = '☆'
    }

    fun toEntity(button: Buttons): ButtonsEntity {
        return ButtonsEntity(symbol = button.symbol, type = button.toEntityType())
    }

    fun toUi(currencies: List<CurrencyEntity>, calculatedData: CalculatedDataEntity): List<CurrencyUi> {
        return currencies.map { toUi(it, calculatedData) }
    }

    fun toSettingsEntity(currencies: List<CurrencyEntity>): List<BaseSettingsEntity> {
        var oldCode = FAVORITE_SYMBOL
        val items = mutableListOf<BaseSettingsEntity>()

        currencies.forEach { entity ->
            val newCode = entity.code.first()

            if (oldCode != newCode) {
                items.add(createHeaderSettingsEntity(newCode))
                oldCode = newCode
            }

            items.add(entity.toSettingsEntity())
        }
        return items
    }

    fun toSettingsEntityWithFavorites(currencies: List<CurrencyEntity>): List<BaseSettingsEntity> {
        return getFavorites(currencies) + toSettingsEntity(currencies)
    }

    private fun toUi(currency: CurrencyEntity, calculatedData: CalculatedDataEntity): CurrencyUi {
        return CurrencyUi(currency.id,
                currency.code,
                currency.name,
                -1, // Don't use flags now: getFlagResourceWithName(currency.flag)
                currency.price,
                currency.isFavorite,
                currency.isSelected,
                currency.favoritePosition,
                calculatedData.toUiCalculatedData(currency.isSelected, currency.price))
    }


    private fun CalculatedDataEntity.toUiCalculatedData(isSelected: Boolean, price: BigDecimal): CalculatedDataEntity {
        val displayValue = if (isSelected) {
            this.displayValue
        } else {
            String.format("%.3f", price)
        }
        return this.copy(displayValue = displayValue)
    }

    private fun getFavorites(currencies: List<CurrencyEntity>): List<BaseSettingsEntity> {
        val favorites = currencies.filter { it.favoritePosition > 0 }.map { it.toSettingsEntity() }

        return if (favorites.isNotEmpty()) {
            listOf(createHeaderSettingsEntity(FAVORITE_SYMBOL)) + favorites
        } else {
            listOf()
        }
    }

    private fun Buttons.toEntityType() = when (this) {
        Buttons.COMMA -> COMMA
        Buttons.BACKSPACE -> BACKSPACE
        Buttons.LONG_BACKSPACE -> LONG_BACKSPACE
        Buttons.DIVIDE, Buttons.MULTIPLY, Buttons.MINUS, Buttons.PLUS -> OPERATOR
        else -> DIGIT
    }


    private fun CurrencyEntity.toSettingsEntity(): SettingsEntity {
        return SettingsEntity(this.id,
                this.code,
                this.name,
                getFlagResourceWithName(this.flag),
                this.price,
                this.isFavorite,
                this.isSelected,
                this.favoritePosition)
    }

    private fun createHeaderSettingsEntity(title: Char): HeaderSettingsEntity {
        return HeaderSettingsEntity(-1, title.toString())
    }

    private fun getFlagResourceWithName(flagName: String): Int {
        return context.resources.getIdentifier(flagName, "drawable", context.packageName)
    }
}

