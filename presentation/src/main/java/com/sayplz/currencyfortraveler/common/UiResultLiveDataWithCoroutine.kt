package com.sayplz.currencyfortraveler.common

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import com.sayplz.domain.base.Result
import java.util.*

class UiResultLiveDataWithCoroutine<IN : Any, OUT : Any>(
        private val isStartOnObserve: Boolean = true,
        private val isStartOnActive: Boolean = false,
        private val transform: (IN) -> OUT,
        private val exec: suspend () -> Result<IN>) : LiveData<UiResult<OUT>>() {

    private var job: Job? = null
    private val results = LinkedList<UiResult<OUT>>()

    override fun observe(owner: LifecycleOwner, observer: Observer<UiResult<OUT>>) {
        super.observe(owner, observer)
        if (isStartOnObserve) refresh()
    }

    override fun removeObservers(owner: LifecycleOwner) {
        super.removeObservers(owner)
        stop()
    }

    override fun onActive() {
        super.onActive()
        publishResults()
        if (isStartOnActive) refresh()
    }

    override fun onInactive() {
        super.onInactive()
        if (isStartOnActive) stop()
    }

    fun refresh() {
        stop()
        job = createJob()
        job?.invokeOnCompletion { doOnCompletion(it) }
    }

    private fun createJob(): Job {
        return launch(UI) {
            setActiveValue(UiResult.OnStart())
            val result = exec()
            when (result) {
                is Result.Success -> setActiveValue(UiResult.OnSuccess(transform(result.data)))
                is Result.Failure -> setActiveValue(UiResult.OnFailure(result.throwable))
            }
        }
    }

    private fun setActiveValue(result: UiResult<OUT>) {
        results.add(result)
        publishResults()
    }

    private fun publishResults() = launch(UI) {
        val iterator = results.iterator()
        while (iterator.hasNext()) {
            if (hasActiveObservers()) {
                value = iterator.next()
                iterator.remove()
            } else {
                break
            }
        }
    }

    private fun doOnCompletion(throwable: Throwable?) {
        if (throwable != null) {
            setActiveValue(UiResult.OnFailure(throwable))
        } else {
            setActiveValue(UiResult.OnFinish(job?.isCancelled ?: false))
        }
    }

    fun stop() {
        results.clear()
        job?.cancel()
    }
}
