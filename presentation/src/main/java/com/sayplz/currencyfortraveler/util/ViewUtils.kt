package com.sayplz.currencyfortraveler.util

import android.support.design.widget.BottomSheetBehavior
import android.support.v4.view.ViewPager
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

inline fun <reified T> RecyclerView.init(
        adapter: ListAdapter<T, RecyclerView.ViewHolder>,
        layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context),
        itemAnimator: RecyclerView.ItemAnimator = DefaultItemAnimator(),
        hasFixedSize: Boolean = true) {
    this.adapter = adapter
    this.layoutManager = layoutManager
    this.itemAnimator = itemAnimator
    setHasFixedSize(hasFixedSize)
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun ViewGroup?.inflate(layoutRes: Int, root: ViewGroup? = null, attacheToRoot: Boolean = false): View {
    return LayoutInflater.from(this?.context).inflate(layoutRes, root, attacheToRoot)
}

fun <V : View?> BottomSheetBehavior<V>.setCallbacks(
        onSlide: (View, Float) -> Unit = { view, offset -> },
        onStateChanged: (View, Int) -> Unit = { view, state -> }) {
    this.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
        override fun onSlide(bottomSheet: View, slideOffset: Float) {
            onSlide(bottomSheet, slideOffset)
        }

        override fun onStateChanged(bottomSheet: View, newState: Int) {
            onStateChanged(bottomSheet, newState)
        }
    })
}

fun ViewPager.onPageChangeListener(
        onPageSelected: (Int) -> Unit = { position -> },
        onPageScrolled: (Int, Float, Int) -> Unit = { position, positionOffset, positionOffsetPixels -> },
        onPageScrollStateChanged: (Int) -> Unit = { state -> }) {
    this.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
        override fun onPageSelected(position: Int) {
            onPageSelected(position)
        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            onPageScrolled(position, positionOffset, positionOffsetPixels)
        }

        override fun onPageScrollStateChanged(state: Int) {
            onPageScrollStateChanged(state)
        }
    })
}

fun View.setMargins(left: Int? = null, top: Int? = null, right: Int? = null, bottom: Int? = null) {
    this.layoutParams = (this.layoutParams as ViewGroup.MarginLayoutParams).apply {
        setMargins(left ?: leftMargin, top ?: topMargin,
                right ?: rightMargin, bottom ?: bottomMargin)
    }
}
