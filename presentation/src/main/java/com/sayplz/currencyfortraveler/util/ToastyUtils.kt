package com.sayplz.currencyfortraveler.util

import android.content.Context
import android.support.annotation.StringRes
import android.widget.Toast
import es.dmoral.toasty.Toasty

/**
 * Created by Sergei Kurikalov
 * on 24.05.18.
 */

fun Context.toastSuccess(@StringRes messageRes: Int) {
    Toasty.success(this, string(messageRes), Toast.LENGTH_SHORT, true).show()
}

fun Context.toastError(@StringRes messageRes: Int) {
    Toasty.error(this, string(messageRes), Toast.LENGTH_SHORT, true).show()
}

fun Context.toastInfo(@StringRes messageRes: Int) {
    Toasty.info(this, string(messageRes), Toast.LENGTH_SHORT, true).show()
}
