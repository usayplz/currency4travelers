package com.sayplz.currencyfortraveler.util

import android.databinding.BindingAdapter
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView


inline fun <reified T : ViewDataBinding> ViewGroup?.databind(layoutRes: Int, attacheToRoot: Boolean = false): T {
    return DataBindingUtil.inflate(LayoutInflater.from(this?.context), layoutRes, this, attacheToRoot) as T
}

@BindingAdapter("imageResource")
fun setImageResource(imageView: ImageView, resource: Int) {
    imageView.setImageResource(resource)
}
