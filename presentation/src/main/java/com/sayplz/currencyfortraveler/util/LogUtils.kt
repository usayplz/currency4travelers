package com.sayplz.currencyfortraveler.util

import android.util.Log

private const val TAG_NAME_LIMIT = 23

fun Any.info(output: Any?, e: Exception? = null) {
    val tag = getTag(this::class.java.simpleName)
    if (Log.isLoggable(tag, Log.INFO)) {
        Log.i(tag, output?.toString() ?: "null", e)
    }
}

private fun getTag(tag: String): String {
    return if (tag.length <= TAG_NAME_LIMIT) tag else tag.take(TAG_NAME_LIMIT)
}
