package com.sayplz.currencyfortraveler.util

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer

fun <T> LiveData<T>.observe(owner: LifecycleOwner, function: (T?) -> Unit) {
    this.observe(owner, Observer { function(it) })
}
