package com.sayplz.domain.repository

import com.sayplz.domain.base.Result

interface SessionRepository {
    suspend fun getLastUpdate(): Result<Long>
}
