package com.sayplz.domain.repository

import com.sayplz.domain.base.Result
import com.sayplz.domain.entity.ButtonsEntity
import com.sayplz.domain.entity.CalculatedDataEntity
import com.sayplz.domain.entity.CurrencyEntity

/**
 * Created by Sergei Kurikalov
 * on 18.03.18.
 */
interface CurrencyRepository {
    suspend fun getFavorites(): Result<List<CurrencyEntity>>
    suspend fun search(query: String): Result<List<CurrencyEntity>>
    suspend fun addButton(button: ButtonsEntity): CalculatedDataEntity
    suspend fun selectCurrency(currencyId: Long)
    suspend fun addFavorite(id: Long)
    suspend fun removeFavorite(id: Long)
    suspend fun swapFavoritePositions(fromId: Long, toId: Long)
    suspend fun updateRatio(): Result<Boolean>
    fun getInitialCalculatedData(): CalculatedDataEntity
}
