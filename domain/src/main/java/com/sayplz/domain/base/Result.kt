package com.sayplz.domain.base

/**
 * Created by Sergei Kurikalov
 * on 19.03.18.
 */
sealed class Result<out T : Any> {
    class Success<out T : Any>(val data: T) : Result<T>()
    class Failure(val throwable: Throwable?) : Result<Nothing>()
}

fun emptyUnit() {}