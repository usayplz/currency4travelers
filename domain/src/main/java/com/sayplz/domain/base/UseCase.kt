package com.sayplz.domain.base

/**
 * Created by Sergei Kurikalov
 * on 18.03.18.
 */
abstract class UseCase<in Params, out Result> {
    internal suspend abstract fun buildUseCase(params: Params?): Result

    suspend fun execute(params: Params? = null): Result {
        return buildUseCase(params)
    }
}

