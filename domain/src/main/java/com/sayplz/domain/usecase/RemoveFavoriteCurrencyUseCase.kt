package com.sayplz.domain.usecase

import com.sayplz.domain.base.UseCase
import com.sayplz.domain.repository.CurrencyRepository

/**
 * Created by Sergei Kurikalov
 * on 24.03.18.
 */
class RemoveFavoriteCurrencyUseCase(private val currencyRepository: CurrencyRepository) : UseCase<Long, Unit>() {
    override suspend fun buildUseCase(currencyId: Long?) {
        if (currencyId != null) {
            return currencyRepository.removeFavorite(currencyId)
        } else {
            throw IllegalArgumentException("Currecny ID must be provided.")
        }
    }
}
