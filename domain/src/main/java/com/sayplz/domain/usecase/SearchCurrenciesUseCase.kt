package com.sayplz.domain.usecase

import com.sayplz.domain.base.Result
import com.sayplz.domain.base.UseCase
import com.sayplz.domain.entity.CurrencyEntity
import com.sayplz.domain.repository.CurrencyRepository

class SearchCurrenciesUseCase(private val currencyRepository: CurrencyRepository) : UseCase<String, Result<List<CurrencyEntity>>>() {
    override suspend fun buildUseCase(query: String?): Result<List<CurrencyEntity>> {
        return currencyRepository.search(query ?: "")
    }
}
