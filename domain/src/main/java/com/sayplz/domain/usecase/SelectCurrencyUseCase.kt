package com.sayplz.domain.usecase

import com.sayplz.domain.base.UseCase
import com.sayplz.domain.repository.CurrencyRepository

class SelectCurrencyUseCase(private val currencyRepository: CurrencyRepository) : UseCase<Long, Unit>() {
    override suspend fun buildUseCase(currencyId: Long?) {
        if (currencyId != null) {
            return currencyRepository.selectCurrency(currencyId)
        } else {
            throw IllegalArgumentException("Currecny ID must be provided.")
        }
    }
}
