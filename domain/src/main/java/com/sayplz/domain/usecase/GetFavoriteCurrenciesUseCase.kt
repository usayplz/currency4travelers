package com.sayplz.domain.usecase

import com.sayplz.domain.base.Result
import com.sayplz.domain.base.UseCase
import com.sayplz.domain.entity.CurrencyEntity
import com.sayplz.domain.repository.CurrencyRepository

/**
 * Created by Sergei Kurikalov
 * on 18.03.18.
 */
class GetFavoriteCurrenciesUseCase(private val currencyRepository: CurrencyRepository) :
        UseCase<Unit, Result<List<CurrencyEntity>>>() {
    override suspend fun buildUseCase(params: Unit?): Result<List<CurrencyEntity>> {
        return currencyRepository.getFavorites()
    }
}
