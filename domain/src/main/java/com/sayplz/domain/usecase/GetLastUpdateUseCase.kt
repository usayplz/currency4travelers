package com.sayplz.domain.usecase

import com.sayplz.domain.base.Result
import com.sayplz.domain.base.UseCase
import com.sayplz.domain.repository.SessionRepository

class GetLastUpdateUseCase(private val sessionRepository: SessionRepository) : UseCase<Unit, Result<Long>>() {
    override suspend fun buildUseCase(params: Unit?): Result<Long> {
        return sessionRepository.getLastUpdate()
    }
}
