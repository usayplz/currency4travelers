package com.sayplz.domain.usecase

import com.sayplz.domain.entity.CalculatedDataEntity
import com.sayplz.domain.repository.CurrencyRepository

/**
 * Created by Sergei Kurikalov
 * on 15.05.18.
 */
class GetInitialCalculatedDataUseCase(private val currencyRepository: CurrencyRepository) {
    fun execute(): CalculatedDataEntity {
        return currencyRepository.getInitialCalculatedData()
    }
}