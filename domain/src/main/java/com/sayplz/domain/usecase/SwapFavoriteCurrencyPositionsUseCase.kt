package com.sayplz.domain.usecase

import com.sayplz.domain.base.UseCase
import com.sayplz.domain.repository.CurrencyRepository

class SwapFavoriteCurrencyPositionsUseCase(private val currencyRepository: CurrencyRepository) : UseCase<Pair<Long, Long>, Unit>() {
    override suspend fun buildUseCase(positions: Pair<Long, Long>?) {
        return if (positions != null) {
            currencyRepository.swapFavoritePositions(positions.first, positions.second)
        } else {
            throw IllegalArgumentException("Currecny IDS must be provided.")
        }
    }
}
