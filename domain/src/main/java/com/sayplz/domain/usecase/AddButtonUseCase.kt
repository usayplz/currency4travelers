package com.sayplz.domain.usecase

import com.sayplz.domain.base.UseCase
import com.sayplz.domain.entity.ButtonsEntity
import com.sayplz.domain.entity.CalculatedDataEntity
import com.sayplz.domain.repository.CurrencyRepository

class AddButtonUseCase(private val currencyRepository: CurrencyRepository) : UseCase<ButtonsEntity, CalculatedDataEntity>() {
    override suspend fun buildUseCase(button: ButtonsEntity?): CalculatedDataEntity {
        if (button != null) {
            return currencyRepository.addButton(button)
        } else {
            throw IllegalArgumentException("ButtonEntity must be provided.")
        }
    }
}
