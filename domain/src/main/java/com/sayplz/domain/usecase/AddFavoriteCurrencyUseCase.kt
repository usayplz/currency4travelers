package com.sayplz.domain.usecase

import com.sayplz.domain.base.UseCase
import com.sayplz.domain.repository.CurrencyRepository

/**
 * Created by Sergei Kurikalov
 * on 24.03.18.
 */
class AddFavoriteCurrencyUseCase(private val currencyRepository: CurrencyRepository) : UseCase<Long?, Unit>() {
    override suspend fun buildUseCase(id: Long?) {
        return if (id != null) {
            currencyRepository.addFavorite(id)
        } else {
            throw IllegalArgumentException("Currecny ID must be provided.")
        }
    }
}
