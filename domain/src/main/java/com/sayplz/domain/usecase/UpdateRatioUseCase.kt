package com.sayplz.domain.usecase

import com.sayplz.domain.base.Result
import com.sayplz.domain.base.UseCase
import com.sayplz.domain.repository.CurrencyRepository

class UpdateRatioUseCase(private val currencyRepository: CurrencyRepository) : UseCase<Unit, Result<Boolean>>() {
    override suspend fun buildUseCase(params: Unit?): Result<Boolean> {
        return currencyRepository.updateRatio()
    }
}
