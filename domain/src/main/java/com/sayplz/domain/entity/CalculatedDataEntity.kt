package com.sayplz.domain.entity

import java.math.BigDecimal

data class CalculatedDataEntity(
        val expression: String,
        val displayValue: String,
        val initialValue: String,
        val value: BigDecimal
)
