package com.sayplz.domain.entity

import java.math.BigDecimal

/**
 * Created by Sergei Kurikalov
 * on 18.03.18.
 */
data class CurrencyEntity(
        val id: Long,
        val code: String,
        val name: String,
        val flag: String,
        val price: BigDecimal,
        val isFavorite: Boolean,
        val isSelected: Boolean,
        val favoritePosition: Long
)
