package com.sayplz.domain.entity

data class ButtonsEntity(
        val symbol: String,
        val type: ButtonsTypeEntity
)
