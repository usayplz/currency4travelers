package com.sayplz.domain.entity

enum class ButtonsTypeEntity {
    DIGIT, OPERATOR, COMMA, BACKSPACE, LONG_BACKSPACE
}
