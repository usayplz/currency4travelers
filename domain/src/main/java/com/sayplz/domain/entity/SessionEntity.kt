package com.sayplz.domain.entity

import java.util.*

data class SessionEntity(
        val lastUpdate: Date
)