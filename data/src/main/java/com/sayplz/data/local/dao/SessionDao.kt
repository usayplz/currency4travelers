package com.sayplz.data.local.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query

@Dao
interface SessionDao {
    @Query("UPDATE session SET lastUpdate = :date")
    fun setLastUpdate(date: Long)

    @Query("SELECT lastUpdate FROM session")
    fun getLastUpdate(): Long
}
