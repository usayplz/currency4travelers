package com.sayplz.data.local.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import android.arch.persistence.room.Update
import com.sayplz.data.local.model.CurrencyLocal

@Dao
abstract class CurrencyDao {
    @Query("SELECT * FROM currencies WHERE isFavorite = 1 ORDER BY favoritePosition DESC")
    abstract fun getFavorites(): List<CurrencyLocal>

    @Query("SELECT MAX(favoritePosition) + 1 FROM currencies WHERE isFavorite = 1")
    abstract fun getNextPosition(): Long

    @Query("SELECT * FROM currencies")
    abstract fun getAll(): List<CurrencyLocal>

    @Query("SELECT * FROM currencies WHERE INSTR(UPPER(code || ' ' || name), UPPER(:query)) > 0 ORDER BY code")
    abstract fun search(query: String): List<CurrencyLocal>

    @Query("UPDATE currencies SET isFavorite = 1, favoritePosition = :nextPosition WHERE id = :id")
    abstract fun addFavorite(id: Long, nextPosition: Long)

    @Update
    abstract fun update(currencyLocal: CurrencyLocal)

    @Query("UPDATE currencies SET isFavorite = 0, isSelected = 0 WHERE id = :id")
    abstract fun removeFavorite(id: Long)

    @Transaction
    open fun swapFavoritePositions(fromId: Long, toId: Long) {
        val from = getCurrencyWithId(fromId)
        val to = getCurrencyWithId(toId)
        if (from != null && to != null) {
            update(from.copy(favoritePosition = to.favoritePosition))
            update(to.copy(favoritePosition = from.favoritePosition))
        }
    }

    @Query("SELECT * FROM currencies WHERE id = :id")
    abstract fun getCurrencyWithId(id: Long): CurrencyLocal?

    @Query("UPDATE currencies SET isSelected = 1 WHERE id = :id")
    abstract fun selectWithId(id: Long)

    @Query("UPDATE currencies SET isSelected = 0")
    abstract fun clearSelected()

    @Transaction
    open fun select(id: Long) {
        clearSelected()
        selectWithId(id)
    }

    @Transaction
    open fun updateRatios(rates: HashMap<String, Double>) {
        getAll().forEach { currency ->
            if (rates.containsKey(currency.code)) {
                rates.get(currency.code)?.let { update(currency.copy(ratio = it)) }
            }
        }
    }

    @Query("SELECT * FROM currencies WHERE isSelected = 1")
    abstract fun getSelected(): CurrencyLocal?
}
