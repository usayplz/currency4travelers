package com.sayplz.data.local

import kotlinx.coroutines.experimental.withContext
import com.sayplz.data.common.Executors

class DiskSessionLocalSource(database: DiskDatabase,
                             private val executors: Executors) {

    private val sessionDao = database.getSessionDao()

    suspend fun setLastUpdate(lastUpdate: Long) = withContext(executors.ioContext) {
        sessionDao.setLastUpdate(lastUpdate)
    }

    suspend fun getLastUpdate(): Long = withContext(executors.ioContext) {
        sessionDao.getLastUpdate()
    }
}
