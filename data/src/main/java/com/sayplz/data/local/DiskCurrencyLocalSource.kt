package com.sayplz.data.local

import kotlinx.coroutines.experimental.withContext
import com.sayplz.data.common.Executors
import com.sayplz.data.local.model.CurrencyLocal

/**
 * Created by Sergei Kurikalov
 * on 27.03.18.
 */
class DiskCurrencyLocalSource(database: DiskDatabase,
                              private val executors: Executors) {

    private val currencyDao = database.getCurrencyDao()

    suspend fun getFavorites() = withContext(executors.ioContext) {
        currencyDao.getFavorites()
    }

    suspend fun addFavorite(id: Long) = withContext(executors.ioContext) {
        currencyDao.addFavorite(id, currencyDao.getNextPosition())
        if (getSelected() == null) {
            selectCurrency(id)
        }
    }

    suspend fun removeFavorite(id: Long) = withContext(executors.ioContext) {
        currencyDao.removeFavorite(id)
        if (getSelected() == null) {
            getFavorites().firstOrNull()?.let {
                selectCurrency(it.id!!)
            }
        }
    }

    suspend fun search(query: String) = withContext(executors.ioContext) {
        currencyDao.search(query)
    }

    suspend fun swapFavoritePositions(fromId: Long, toId: Long) = withContext(executors.ioContext) {
        currencyDao.swapFavoritePositions(fromId, toId)
    }

    suspend fun selectCurrency(id: Long) = withContext(executors.ioContext) {
        currencyDao.select(id)
    }

    suspend fun updateRatios(rates: HashMap<String, Double>) = withContext(executors.ioContext) {
        currencyDao.updateRatios(rates)
    }

    suspend fun getSelected(): CurrencyLocal? = withContext(executors.ioContext) {
        currencyDao.getSelected()
    }
}
