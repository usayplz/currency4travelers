package com.sayplz.data.local.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Sergei Kurikalov
 * on 19.03.18.
 */
@Entity(tableName = "currencies")
data class CurrencyLocal(
        @PrimaryKey(autoGenerate = true) val id: Long? = null,
        val code: String,
        val name: String,
        val flag: String,
        val ratio: Double,
        val isFavorite: Boolean,
        val isSelected: Boolean,
        val favoritePosition: Long
)
