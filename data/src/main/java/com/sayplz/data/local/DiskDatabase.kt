package com.sayplz.data.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.sayplz.data.common.Constants
import com.sayplz.data.local.dao.CurrencyDao
import com.sayplz.data.local.dao.SessionDao
import com.sayplz.data.local.model.CurrencyLocal
import com.sayplz.data.local.model.SessionLocal

/**
 * Created by Sergei Kurikalov
 * on 19.03.18.
 */
@Database(entities = arrayOf(CurrencyLocal::class, SessionLocal::class), version = Constants.DATABASE_VERSION)
abstract class DiskDatabase : RoomDatabase() {
    abstract fun getCurrencyDao(): CurrencyDao
    abstract fun getSessionDao(): SessionDao
}
