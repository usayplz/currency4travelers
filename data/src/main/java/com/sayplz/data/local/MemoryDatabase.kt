package com.sayplz.data.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.sayplz.data.common.Constants
import com.sayplz.data.local.dao.CurrencyDao
import com.sayplz.data.local.model.CurrencyLocal

/**
 * Created by Sergei Kurikalov
 * on 28.03.18.
 */
@Database(entities = arrayOf(CurrencyLocal::class), version = Constants.DATABASE_VERSION)
abstract class MemoryDatabase : RoomDatabase() {
    abstract fun getCurrencyDao(): CurrencyDao
}
