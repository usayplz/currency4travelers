package com.sayplz.data.local

import com.sayplz.data.common.Executors

/**
 * Created by Sergei Kurikalov
 * on 28.03.18.
 */
class MemoryCurrencyLocalSource(cache: MemoryDatabase,
                                private val executors: Executors) {

    private val currencyDao = cache.getCurrencyDao()
}
