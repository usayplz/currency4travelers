package com.sayplz.data.local.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "session")
data class SessionLocal(@PrimaryKey val lastUpdate: Long)