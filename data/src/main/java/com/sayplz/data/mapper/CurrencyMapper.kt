package com.sayplz.data.mapper

import com.sayplz.data.local.model.CurrencyLocal
import com.sayplz.domain.entity.CurrencyEntity
import java.math.BigDecimal

/**
 * Created by Sergei Kurikalov
 * on 27.03.18.
 */
object CurrencyMapper {
    fun toEntity(currencyLocal: CurrencyLocal, price: BigDecimal, baseRatio: BigDecimal): CurrencyEntity {
        return CurrencyEntity(
                currencyLocal.id ?: -1,
                currencyLocal.code,
                currencyLocal.name,
                currencyLocal.flag,
                if (baseRatio > BigDecimal.ZERO) currencyLocal.ratio.toBigDecimal().times(price).div(baseRatio) else BigDecimal.ZERO,
                currencyLocal.isFavorite,
                currencyLocal.isSelected,
                currencyLocal.favoritePosition
        )
    }

    fun toEntity(currencyLocals: List<CurrencyLocal>?, price: BigDecimal, baseRatio: BigDecimal): List<CurrencyEntity> {
        return currencyLocals?.map { toEntity(it, price, baseRatio) } ?: listOf()
    }
}
