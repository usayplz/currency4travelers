package com.sayplz.data

import com.sayplz.data.local.DiskSessionLocalSource
import com.sayplz.domain.base.Result
import com.sayplz.domain.repository.SessionRepository

class SessionRepositoryImpl(private val diskSessionLocalSource: DiskSessionLocalSource) : SessionRepository {
    override suspend fun getLastUpdate(): Result<Long> {
        val lastUpdate = diskSessionLocalSource.getLastUpdate()
        if (lastUpdate > 0) {
            return Result.Success(lastUpdate)
        } else {
            return Result.Failure(IllegalAccessError())
        }
    }
}
