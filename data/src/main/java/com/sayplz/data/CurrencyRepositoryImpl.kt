package com.sayplz.data

import com.sayplz.data.calculator.Calculator
import com.sayplz.data.exception.FirstUpdateException
import com.sayplz.data.local.DiskCurrencyLocalSource
import com.sayplz.data.local.DiskSessionLocalSource
import com.sayplz.data.local.MemoryCurrencyLocalSource
import com.sayplz.data.mapper.CurrencyMapper
import com.sayplz.data.remote.FirebaseCurrencySource
import com.sayplz.data.remote.model.CurrencyRemote
import com.sayplz.domain.base.Result
import com.sayplz.domain.entity.ButtonsEntity
import com.sayplz.domain.entity.CalculatedDataEntity
import com.sayplz.domain.entity.CurrencyEntity
import com.sayplz.domain.repository.CurrencyRepository
import kotlinx.coroutines.experimental.launch
import java.math.BigDecimal
import java.util.*
import kotlin.coroutines.experimental.Continuation
import kotlin.coroutines.experimental.suspendCoroutine

/**
 * Created by Sergei Kurikalov
 * on 19.03.18.
 */

class CurrencyRepositoryImpl(private val diskCurrencyLocalSource: DiskCurrencyLocalSource,
                             private val memoryCurrencyLocalSource: MemoryCurrencyLocalSource,
                             private val diskSessionLocalSource: DiskSessionLocalSource,
                             private val firebaseCurrencySource: FirebaseCurrencySource) : CurrencyRepository {

    private val mapper = CurrencyMapper
    private val calculator = Calculator(DEFAULT_PRICE)

    override fun getInitialCalculatedData(): CalculatedDataEntity {
        calculator.reset()
        return calculator.data
    }

    override suspend fun getFavorites(): Result<List<CurrencyEntity>> {
        return try {
            val favorites = diskCurrencyLocalSource.getFavorites()
            Result.Success(mapper.toEntity(favorites, calculator.data.value, getBaseRatio()))
        } catch (e: Exception) {
            Result.Failure(IllegalArgumentException(e))
        }
    }

    override suspend fun selectCurrency(id: Long) {
        diskCurrencyLocalSource.selectCurrency(id)
        calculator.reset()
    }

    override suspend fun search(query: String): Result<List<CurrencyEntity>> {
        return try {
            Result.Success(mapper.toEntity(diskCurrencyLocalSource.search(query), calculator.data.value, getBaseRatio()))
        } catch (e: Exception) {
            Result.Failure(IllegalArgumentException(e))
        }
    }

    override suspend fun addFavorite(id: Long) {
        diskCurrencyLocalSource.addFavorite(id)
    }

    override suspend fun removeFavorite(id: Long) {
        diskCurrencyLocalSource.removeFavorite(id)
    }

    override suspend fun swapFavoritePositions(fromId: Long, toId: Long) {
        try {
            diskCurrencyLocalSource.swapFavoritePositions(fromId, toId)
        } catch (ignore: Exception) {
        }
    }

    override suspend fun addButton(button: ButtonsEntity): CalculatedDataEntity {
        calculator.addButton(button)
        return calculator.data
    }

    override suspend fun updateRatio(): Result<Boolean> = suspendCoroutine { cont ->
        firebaseCurrencySource.connect(
                onSuccess = { firebaseOnSuccess(it, cont) },
                onError = { firebaseOnFailure(it, cont) })
    }

    private fun firebaseOnFailure(e: Exception, cont: Continuation<Result<Boolean>>) = launch {
        if (diskSessionLocalSource.getLastUpdate() == 0L) {
            cont.resume(Result.Failure(FirstUpdateException()))
        } else {
            cont.resume(Result.Failure(e))
        }
    }

    private fun firebaseOnSuccess(remote: CurrencyRemote, cont: Continuation<Result<Boolean>>) = launch {
        try {
            diskCurrencyLocalSource.updateRatios(remote.rates)
            cont.resume(Result.Success(diskSessionLocalSource.getLastUpdate() == 0L))
            diskSessionLocalSource.setLastUpdate(Date().time)
        } catch (ignored: Exception) {
        }
    }

    private suspend fun getBaseRatio(): BigDecimal {
        return diskCurrencyLocalSource.getSelected()?.ratio?.toBigDecimal() ?: BigDecimal.ZERO
    }

    companion object {
        private const val DEFAULT_PRICE = "1"
    }
}
