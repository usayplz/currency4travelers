package com.sayplz.data.exception

class FirstUpdateException : Throwable("Currencies was never updated")