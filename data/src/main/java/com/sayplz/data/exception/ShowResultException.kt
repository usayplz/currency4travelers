package com.sayplz.data.exception

/**
 * Created by Sergei Kurikalov
 * on 24.05.18.
 */
class ShowResultException(message: String?) : Throwable(message ?: "")
