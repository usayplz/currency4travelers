package com.sayplz.data.calculator

import com.sayplz.domain.entity.ButtonsEntity
import com.sayplz.domain.entity.ButtonsTypeEntity
import com.sayplz.domain.entity.CalculatedDataEntity
import java.math.BigDecimal

class Calculator(private val initialValue: String) {
    companion object {
        private const val EXPRESSION_LENGTH_LIMIT = 50
        private const val PRICE_LENGTH_LIMIT = 10
    }

    var data = getInitialData()

    fun addButton(button: ButtonsEntity): CalculatedDataEntity {
        if (button.type == ButtonsTypeEntity.LONG_BACKSPACE) {
            return getInitialData().also { data = getInitialData() }
        }

        if (insideLimits(button)) {
            val transformer = ExpressionTransformer(data, button, initialValue)
            data = transformer.calculatedData
        }

        return data
    }

    private fun insideLimits(button: ButtonsEntity) =
            (data.expression.length < EXPRESSION_LENGTH_LIMIT
                    && data.displayValue.length < PRICE_LENGTH_LIMIT)
                    || button.type == ButtonsTypeEntity.BACKSPACE

    private fun getInitialData(): CalculatedDataEntity {
        return CalculatedDataEntity("", "", initialValue, BigDecimal(initialValue))
    }

    fun reset() {
        data = getInitialData()
    }
}
