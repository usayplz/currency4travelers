package com.sayplz.data.calculator

import com.sayplz.domain.entity.ButtonsEntity
import com.sayplz.domain.entity.CalculatedDataEntity
import com.udojava.evalex.Expression
import java.math.BigDecimal
import java.text.DecimalFormatSymbols
import java.util.*

class ExpressionTransformer(data: CalculatedDataEntity, button: ButtonsEntity, initialValue: String) {
    var calculatedData: CalculatedDataEntity

    init {
        var nextValue = if (data.expression.isEmpty()) {
            data.displayValue
        } else {
            data.expression
        }

        nextValue = getNextValueWithButton(nextValue, button)

        val expression = if (nextValue.isExpression()) nextValue else ""
        val displayValue = if (nextValue.isExpression()) calc(expression) else nextValue
        val newInitialValue = getInitialValue(displayValue, initialValue)
        val value = getValue(displayValue, initialValue)

        calculatedData = CalculatedDataEntity(expression, displayValue, newInitialValue, value)
    }

    private fun getValue(displayValue: String, initialValue: String): BigDecimal = when (displayValue) {
        "" -> BigDecimal(initialValue)
        else -> BigDecimal(displayValue.replace(",", "."))
    }

    private fun getNextValueWithButton(nextValue: String, button: ButtonsEntity): String {
        val buttonsTypeData = ButtonsTypeData.valueOf(button.type.name)
        return buttonsTypeData.add(nextValue, button.symbol)
    }

    private fun getInitialValue(displayValue: String, initialValue: String) = when (displayValue.isEmpty()) {
        true -> initialValue
        false -> ""
    }

    private fun calc(expression: String): String = try {
        val clean = clean(expression)
        Expression(clean).eval().toLocalString()
    } catch (e: Exception) {
        "0"
    }

    private fun clean(expression: String): String {
        val cleaned = expression
                .replace(" ", "")
                .replace("÷", "/")
                .replace("×", "*")
                .replace(",", ".")

        return if (Regex(".*[+\\-/*]+$").matches(cleaned)) {
            cleaned.dropLast(1)
        } else {
            cleaned
        }
    }

    enum class ButtonsTypeData {
        DIGIT {
            override fun add(expression: String, symbol: String) = if (expression.getEnd() == "0") {
                symbol
            } else {
                expression.plus(symbol)
            }
        },

        OPERATOR {
            override fun add(expression: String, symbol: String): String {
                return if (expression.isEmpty()) {
                    ""
                } else if (expression.endsWithOperator()) {
                    expression.dropLast(3).plus(" $symbol ")
                } else if (expression.endsWith(".") || expression.endsWith(",")) {
                    expression.dropLast(1).plus(" $symbol ")
                } else {
                    expression.plus(" $symbol ")
                }
            }
        },

        COMMA {
            override fun add(expression: String, symbol: String): String = when {
                expression.getEnd().isEmpty() -> expression.plus("0$symbol")
                expression.getEnd().contains(symbol) -> expression
                else -> expression.plus(symbol)
            }
        },

        BACKSPACE {
            override fun add(expression: String, symbol: String): String {
                val dropCount = when {
                    expression.endsWithOperator() -> 3
                    expression.isEmpty() -> 0
                    else -> 1
                }
                return expression.dropLast(dropCount)
            }
        };

        abstract fun add(expression: String, symbol: String): String

        fun String.endsWithOperator(): Boolean {
            return matches(Regex(".*[+\\-÷×]+\\s$"))
        }

        fun String.getEnd(): String = if (this.isExpression()) {
            this.substring(this.lastIndexOf(" ") + 1, this.length)
        } else {
            this
        }
    }
}

private val DECIMAL_SEPARATOR: String = DecimalFormatSymbols(Locale.getDefault()).decimalSeparator.toString()

private fun BigDecimal.toLocalString(): String {
    return this.toPlainString().replace(".", DECIMAL_SEPARATOR)
}

private fun String.isExpression(): Boolean {
    return this.matches(Regex(".*[+\\-÷×]+.*"))
}