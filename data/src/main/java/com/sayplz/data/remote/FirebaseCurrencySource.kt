package com.sayplz.data.remote

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.sayplz.data.remote.model.CurrencyRemote

/**
 * Created by Sergei Kurikalov
 * on 27.03.18.
 */
class FirebaseCurrencySource() {
    companion object {
        private const val USERNAME = "u.sayplz@gmail.com"
        private const val PASSWORD = "s5hmQMJJPWfTbaskPPbwmKY4Bls23"
        private const val DATABASE_PATH = "currency"
    }

    fun connect(onSuccess: (CurrencyRemote) -> Unit, onError: (Exception) -> Unit) {
        FirebaseAuth.getInstance().apply {
            signInWithEmailAndPassword(USERNAME, PASSWORD)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            read(onSuccess, onError)
                        }
                    }
                    .addOnFailureListener { onError(it) }
        }
    }

    private fun read(onSuccess: (CurrencyRemote) -> Unit, onError: (Exception) -> Unit) {
        val database = FirebaseDatabase.getInstance()
        val reference = database.getReference(DATABASE_PATH)

        reference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError?) {
                onError(error?.toException() ?: Exception("Unknown Error"))
            }

            override fun onDataChange(dataSnapshot: DataSnapshot?) {
                dataSnapshot?.getValue(CurrencyRemote::class.java)?.let { onSuccess(it) }
            }
        })
    }
}
