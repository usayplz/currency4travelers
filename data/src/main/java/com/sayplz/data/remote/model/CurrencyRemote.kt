package com.sayplz.data.remote.model

/**
 * Created by Sergei Kurikalov
 * on 29.03.18.
 */
data class CurrencyRemote(
        val base: String = "",
        val timestamp: Long = 0,
        val rates: HashMap<String, Double> = HashMap()
)
