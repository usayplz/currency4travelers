package com.sayplz.data.common

object Constants {
    const val DATABASE_NAME = "currency.db"
    const val DATABASE_VERSION = 1
}