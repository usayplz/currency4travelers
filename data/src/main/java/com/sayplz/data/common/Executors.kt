package com.sayplz.data.common

import kotlinx.coroutines.experimental.DefaultDispatcher
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.newFixedThreadPoolContext
import kotlin.coroutines.experimental.CoroutineContext

/**
 * Created by Sergei Kurikalov
 * on 29.03.18.
 */
private const val THREAD_COUNT = 5
private const val THREAD_NAME = "networkIO"

class Executors constructor(
        val ioContext: CoroutineContext = DefaultDispatcher,
        val networkContext: CoroutineContext = newFixedThreadPoolContext(THREAD_COUNT, THREAD_NAME),
        val uiContext: CoroutineContext = UI)
