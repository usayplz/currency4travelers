package com.sayplz.data.common

inline fun <E : Any, T : Collection<E>> T?.switchIfNullOrEmpty(func: () -> T?): T? {
    return if (this == null || this.isEmpty()) func() else return this
}

inline fun <E : Any, T : Collection<E>> T.switchIfEmpty(func: () -> T): T {
    return if (this.isEmpty()) func() else this
}
