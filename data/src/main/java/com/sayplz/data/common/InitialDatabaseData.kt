package com.sayplz.data.common

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.RoomDatabase
import com.sayplz.data.local.model.CurrencyLocal
import java.util.*

class InitialDatabaseData {
    companion object {
        private const val INITIAL_LAST_UPDATE = 0L
        private const val DEFAULT_SELECTED_CODE = "USD"
        private val DEFAULT_FAVORITE_CODES = listOf("USD", "EUR", "BTC")
    }

    fun onCreateCallback(): RoomDatabase.Callback = object : RoomDatabase.Callback() {
        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            fillCurrencies(db)
            fillSession(db)
        }
    }

    private fun fillCurrencies(db: SupportSQLiteDatabase) {
        val userCode = Currency.getInstance(Locale.getDefault()).currencyCode
        var position = 0L
        getCurrencies().forEach {
            var currency = it
            if (it.code in DEFAULT_FAVORITE_CODES || it.code == userCode) {
                currency = currency.copy(isFavorite = true, favoritePosition = ++position)
            }

            if (it.code == DEFAULT_SELECTED_CODE) {
                currency = currency.copy(isSelected = true)
            }

            db.execSQL(currency.toInsertingString())
        }
    }

    private fun fillSession(db: SupportSQLiteDatabase) {
        db.execSQL("INSERT INTO session VALUES($INITIAL_LAST_UPDATE)")
    }

    private fun CurrencyLocal.toInsertingString(): String {
        val isFavorite = if (this.isFavorite) 1 else 0
        val isSelected = if (this.isSelected) 1 else 0
        return "INSERT INTO currencies VALUES ($id, '$code', '$name', '$flag', $ratio, $isFavorite, $isSelected, $favoritePosition)"
    }

    private fun getCurrencies(): List<CurrencyLocal> = arrayListOf(
            CurrencyLocal(null, "BTC", "Bitcoin", "bc", 0.0, false, false, 0),
            CurrencyLocal(null, "ETH", "Ethereum", "eth", 0.0, false, false, 0),
            CurrencyLocal(null, "AED", "UAE Dirham", "ae", 0.0, false, false, 0),
            CurrencyLocal(null, "AFN", "Afghani", "af", 0.0, false, false, 0),
            CurrencyLocal(null, "ALL", "Albanian Lek", "al", 0.0, false, false, 0),
            CurrencyLocal(null, "AMD", "Armenian Dram", "am", 0.0, false, false, 0),
            CurrencyLocal(null, "AOA", "Angolan Kwanza", "ao", 0.0, false, false, 0),
            CurrencyLocal(null, "ARS", "Argentine Peso", "ar", 0.0, false, false, 0),
            CurrencyLocal(null, "AUD", "Australian Dollar", "au", 0.0, false, false, 0),
            CurrencyLocal(null, "AWG", "Aruban Florin", "aw", 0.0, false, false, 0),
            CurrencyLocal(null, "AZN", "Azerbaijanian Manat", "az", 0.0, false, false, 0),
            CurrencyLocal(null, "BAM", "Convertible Mark", "ba", 0.0, false, false, 0),
            CurrencyLocal(null, "BBD", "Barbados Dollar", "bb", 0.0, false, false, 0),
            CurrencyLocal(null, "BDT", "Bangladeshi Taka", "bd", 0.0, false, false, 0),
            CurrencyLocal(null, "BGN", "Bulgarian Lev", "bg", 0.0, false, false, 0),
            CurrencyLocal(null, "BHD", "Bahraini Dinar", "bh", 0.0, false, false, 0),
            CurrencyLocal(null, "BIF", "Burundi Franc", "bi", 0.0, false, false, 0),
            CurrencyLocal(null, "BMD", "Bermudian Dollar", "bm", 0.0, false, false, 0),
            CurrencyLocal(null, "BND", "Brunei Dollar", "bn", 0.0, false, false, 0),
            CurrencyLocal(null, "BOB", "Bolivian boliviano", "bo", 0.0, false, false, 0),
            CurrencyLocal(null, "BRL", "Brazilian Real", "br", 0.0, false, false, 0),
            CurrencyLocal(null, "BSD", "Bahamian Dollar", "bs", 0.0, false, false, 0),
            CurrencyLocal(null, "BWP", "Botswanan Pula", "bw", 0.0, false, false, 0),
            CurrencyLocal(null, "BYN", "Belarussian Ruble", "by", 0.0, false, false, 0),
            CurrencyLocal(null, "BZD", "Belize Dollar", "bz", 0.0, false, false, 0),
            CurrencyLocal(null, "CAD", "Canadian Dollar", "ca", 0.0, false, false, 0),
            CurrencyLocal(null, "CDF", "Congolese franc", "cd", 0.0, false, false, 0),
            CurrencyLocal(null, "CHF", "Swiss Franc", "ch", 0.0, false, false, 0),
            CurrencyLocal(null, "CLP", "Chilean Peso", "cl", 0.0, false, false, 0),
            CurrencyLocal(null, "CNY", "Chinese Yuan Renminbi", "cn", 0.0, false, false, 0),
            CurrencyLocal(null, "COP", "Colombian Peso", "co", 0.0, false, false, 0),
            CurrencyLocal(null, "CRC", "Costa Rican Colon", "cr", 0.0, false, false, 0),
            CurrencyLocal(null, "CUP", "Cuban Peso", "cu", 0.0, false, false, 0),
            CurrencyLocal(null, "CVE", "Cabo Verde Escudo", "cv", 0.0, false, false, 0),
            CurrencyLocal(null, "CZK", "Czech Koruna", "cz", 0.0, false, false, 0),
            CurrencyLocal(null, "DJF", "Djibouti Franc", "dj", 0.0, false, false, 0),
            CurrencyLocal(null, "DKK", "Danish Krone", "dk", 0.0, false, false, 0),
            CurrencyLocal(null, "DOP", "Dominican peso", "d0", 0.0, false, false, 0),
            CurrencyLocal(null, "DZD", "Algerian Dinar", "dz", 0.0, false, false, 0),
            CurrencyLocal(null, "EGP", "Egyptian Pound", "eg", 0.0, false, false, 0),
            CurrencyLocal(null, "ERN", "Eritrean Nakfa", "er", 0.0, false, false, 0),
            CurrencyLocal(null, "ETB", "Ethiopian Birr", "et", 0.0, false, false, 0),
            CurrencyLocal(null, "EUR", "Euro", "eu", 0.0, false, false, 0),
            CurrencyLocal(null, "FJD", "Fiji Dollar", "fj", 0.0, false, false, 0),
            CurrencyLocal(null, "GBP", "Pound sterling", "gb", 0.0, false, false, 0),
            CurrencyLocal(null, "GEL", "Georgian Lari", "ge", 0.0, false, false, 0),
            CurrencyLocal(null, "GHS", "Ghana Cedi", "gh", 0.0, false, false, 0),
            CurrencyLocal(null, "GIP", "Gibraltar Pound", "gi", 0.0, false, false, 0),
            CurrencyLocal(null, "GMD", "Gambian Dalasi", "gm", 0.0, false, false, 0),
            CurrencyLocal(null, "GNF", "Guinea Franc", "gn", 0.0, false, false, 0),
            CurrencyLocal(null, "GTQ", "Guatemalan Quetzal", "gt", 0.0, false, false, 0),
            CurrencyLocal(null, "GYD", "Guyana Dollar", "gy", 0.0, false, false, 0),
            CurrencyLocal(null, "HKD", "Hong Kong Dollar", "hk", 0.0, false, false, 0),
            CurrencyLocal(null, "HNL", "Honduran Lempira", "hn", 0.0, false, false, 0),
            CurrencyLocal(null, "HRK", "Croatian Kuna", "hr", 0.0, false, false, 0),
            CurrencyLocal(null, "HUF", "Hungarian Forint", "hu", 0.0, false, false, 0),
            CurrencyLocal(null, "IDR", "Indonesian Rupiah", "id", 0.0, false, false, 0),
            CurrencyLocal(null, "ILS", "New Israeli Sheqel", "il", 0.0, false, false, 0),
            CurrencyLocal(null, "INR", "Indian Rupee", "in", 0.0, false, false, 0),
            CurrencyLocal(null, "IQD", "Iraqi Dinar", "iq", 0.0, false, false, 0),
            CurrencyLocal(null, "IRR", "Iranian rial", "ir", 0.0, false, false, 0),
            CurrencyLocal(null, "ISK", "Iceland Krona", "is", 0.0, false, false, 0),
            CurrencyLocal(null, "JMD", "Jamaican Dollar", "jm", 0.0, false, false, 0),
            CurrencyLocal(null, "JOD", "Jordanian Dinar", "jo", 0.0, false, false, 0),
            CurrencyLocal(null, "JPY", "Japanese Yen", "jp", 0.0, false, false, 0),
            CurrencyLocal(null, "KES", "Kenyan Shilling", "ke", 0.0, false, false, 0),
            CurrencyLocal(null, "KGS", "Kyrgystani Som", "kg", 0.0, false, false, 0),
            CurrencyLocal(null, "KHR", "Cambodian Riel", "kh", 0.0, false, false, 0),
            CurrencyLocal(null, "KMF", "Comoro Franc", "km", 0.0, false, false, 0),
            CurrencyLocal(null, "KPW", "North Korean won", "kp", 0.0, false, false, 0),
            CurrencyLocal(null, "KRW", "South Korean Won", "kr", 0.0, false, false, 0),
            CurrencyLocal(null, "KWD", "Kuwaiti Dinar", "kw", 0.0, false, false, 0),
            CurrencyLocal(null, "KZT", "Kazakhstani Tenge", "kz", 0.0, false, false, 0),
            CurrencyLocal(null, "LAK", "Laotian Kip", "la", 0.0, false, false, 0),
            CurrencyLocal(null, "LBP", "Lebanese Pound", "lb", 0.0, false, false, 0),
            CurrencyLocal(null, "LKR", "Sri Lanka Rupee", "lk", 0.0, false, false, 0),
            CurrencyLocal(null, "LRD", "Liberian Dollar", "lr", 0.0, false, false, 0),
            CurrencyLocal(null, "LYD", "Libyan Dinar", "ly", 0.0, false, false, 0),
            CurrencyLocal(null, "MAD", "Moroccan Dirham", "ma", 0.0, false, false, 0),
            CurrencyLocal(null, "MDL", "Moldovan Leu", "md", 0.0, false, false, 0),
            CurrencyLocal(null, "MGA", "Malagasy Ariary", "mg", 0.0, false, false, 0),
            CurrencyLocal(null, "MKD", "Macedonian Denar", "mk", 0.0, false, false, 0),
            CurrencyLocal(null, "MMK", "Myanmar Kyat", "mm", 0.0, false, false, 0),
            CurrencyLocal(null, "MNT", "Mongolian Tugrik", "mn", 0.0, false, false, 0),
            CurrencyLocal(null, "MRO", "Mauritanian Ouguiya", "mr", 0.0, false, false, 0),
            CurrencyLocal(null, "MUR", "Mauritius Rupee", "mu", 0.0, false, false, 0),
            CurrencyLocal(null, "MVR", "Maldivian Rufiyaa", "mv", 0.0, false, false, 0),
            CurrencyLocal(null, "MWK", "Malawian Kwacha", "mw", 0.0, false, false, 0),
            CurrencyLocal(null, "MXN", "Mexican Peso", "mx", 0.0, false, false, 0),
            CurrencyLocal(null, "MYR", "Malaysian Ringgit", "my", 0.0, false, false, 0),
            CurrencyLocal(null, "MZN", "Mozambique Metical", "mz", 0.0, false, false, 0),
            CurrencyLocal(null, "NGN", "Nigerian Naira", "ng", 0.0, false, false, 0),
            CurrencyLocal(null, "NIO", "Cordoba Oro", "ni", 0.0, false, false, 0),
            CurrencyLocal(null, "NOK", "Norwegian Krone", "no", 0.0, false, false, 0),
            CurrencyLocal(null, "NPR", "Nepalese Rupee", "np", 0.0, false, false, 0),
            CurrencyLocal(null, "NZD", "New Zealand Dollar", "nz", 0.0, false, false, 0),
            CurrencyLocal(null, "OMR", "Rial Omani", "om", 0.0, false, false, 0),
            CurrencyLocal(null, "PEN", "Peruvian Nuevo Sol", "pe", 0.0, false, false, 0),
            CurrencyLocal(null, "PGK", "Papua New Guinean Kina", "pg", 0.0, false, false, 0),
            CurrencyLocal(null, "PHP", "Philippine Peso", "ph", 0.0, false, false, 0),
            CurrencyLocal(null, "PKR", "Pakistan Rupee", "pk", 0.0, false, false, 0),
            CurrencyLocal(null, "PLN", "Polish Zloty", "pl", 0.0, false, false, 0),
            CurrencyLocal(null, "PYG", "Paraguayan Guarani", "py", 0.0, false, false, 0),
            CurrencyLocal(null, "QAR", "Qatari Rial", "qa", 0.0, false, false, 0),
            CurrencyLocal(null, "RON", "New Romanian Leu", "ro", 0.0, false, false, 0),
            CurrencyLocal(null, "RUB", "Russian Ruble", "ru", 0.0, false, false, 0),
            CurrencyLocal(null, "RWF", "Rwanda Franc", "rw", 0.0, false, false, 0),
            CurrencyLocal(null, "SAR", "Saudi Riyal", "sa", 0.0, false, false, 0),
            CurrencyLocal(null, "SBD", "Solomon Islands Dollar", "sb", 0.0, false, false, 0),
            CurrencyLocal(null, "SCR", "Seychelles Rupee", "sc", 0.0, false, false, 0),
            CurrencyLocal(null, "SDG", "Sudanese Pound", "sd", 0.0, false, false, 0),
            CurrencyLocal(null, "SEK", "Swedish Krona", "se", 0.0, false, false, 0),
            CurrencyLocal(null, "SGD", "Singapore Dollar", "sg", 0.0, false, false, 0),
            CurrencyLocal(null, "SLL", "Sierra Leonean Leone", "sl", 0.0, false, false, 0),
            CurrencyLocal(null, "SOS", "Somali Shilling", "so", 0.0, false, false, 0),
            CurrencyLocal(null, "SRD", "Surinam Dollar", "sr", 0.0, false, false, 0),
            CurrencyLocal(null, "STD", "Dobra", "st", 0.0, false, false, 0),
            CurrencyLocal(null, "SYP", "Syrian Pound", "sy", 0.0, false, false, 0),
            CurrencyLocal(null, "SZL", "Swazi Lilangeni", "sz", 0.0, false, false, 0),
            CurrencyLocal(null, "THB", "Thai Baht", "th", 0.0, false, false, 0),
            CurrencyLocal(null, "TJS", "Tajikistani Somoni", "tj", 0.0, false, false, 0),
            CurrencyLocal(null, "TMT", "Turkmenistan New Manat", "tm", 0.0, false, false, 0),
            CurrencyLocal(null, "TND", "Tunisian Dinar", "tn", 0.0, false, false, 0),
            CurrencyLocal(null, "TOP", "Tongan Pa’anga", "to", 0.0, false, false, 0),
            CurrencyLocal(null, "TRY", "Turkish Lira", "tr", 0.0, false, false, 0),
            CurrencyLocal(null, "TTD", "Trinidad and Tobago Dollar", "tt", 0.0, false, false, 0),
            CurrencyLocal(null, "TZS", "Tanzanian shilling", "tz", 0.0, false, false, 0),
            CurrencyLocal(null, "UAH", "Ukrainian Hryvnia", "ua", 0.0, false, false, 0),
            CurrencyLocal(null, "UGX", "Uganda Shilling", "ug", 0.0, false, false, 0),
            CurrencyLocal(null, "USD", "US Dollar", "us", 0.0, false, false, 0),
            CurrencyLocal(null, "UYU", "Peso Uruguayo", "uy", 0.0, false, false, 0),
            CurrencyLocal(null, "UZS", "Uzbekistan Sum", "uz", 0.0, false, false, 0),
            CurrencyLocal(null, "VEF", "Venezuelan bolívar", "ve", 0.0, false, false, 0),
            CurrencyLocal(null, "VND", "Vietnamese Dong", "vn", 0.0, false, false, 0),
            CurrencyLocal(null, "VUV", "Vanuatu Vatu", "vu", 0.0, false, false, 0),
            CurrencyLocal(null, "WST", "Samoan Tala", "ws", 0.0, false, false, 0),
            CurrencyLocal(null, "XAF", "CFA Franc BEAC", "cf", 0.0, false, false, 0),
            CurrencyLocal(null, "XCD", "East Caribbean Dollar", "lc", 0.0, false, false, 0),
            CurrencyLocal(null, "XPF", "CFP Franc", "pf", 0.0, false, false, 0),
            CurrencyLocal(null, "YER", "Yemeni Rial", "ye", 0.0, false, false, 0),
            CurrencyLocal(null, "ZAR", "South African Rand", "za", 0.0, false, false, 0),
            CurrencyLocal(null, "ZMW", "Zambian Kwacha", "zm", 0.0, false, false, 0),
            CurrencyLocal(null, "ZWL", "Zimbabwe Dollar", "zw", 0.0, false, false, 0)
    )
}
